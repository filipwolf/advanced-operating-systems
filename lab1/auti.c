#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

#define CARS_TO_WAIT_FOR 3

int timeout = 0;

//message data structure
struct my_msgbuf {
    long mtype;
    char mtext[200];
};

void alarmSignal(int nesto) {
    timeout = 1;
}


//semaphore function that manages cars crossing the bridge
void semafor (int numOfCars) {

    //alarm signal definition
    signal(SIGALRM, alarmSignal);
    srand(getpid());

    //determine direction at random
    int smjer = rand() % 2;
    int cnt = 0;
    struct my_msgbuf buf;
    int msqid, msqid1, msqid2;

    //open message queue in direction a
    if ((msqid1 = msgget(1, 0600 | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }

    //open message queue in direction b
    if ((msqid2 = msgget(2, 0600 | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }

    //loop until all cars have crossed the bridge
    while (cnt < numOfCars) {

        //determine queue used depending on direction
        if (smjer == 0) msqid = msqid1;
        else msqid = msqid2;

        int cntCarsToRelease = 0;
        int msqids[3] = {0};
        int reg_oznake[3] = {0};

        //set alarm
        ualarm(rand() % 500001 + 500000, 0);

        //wait for CARS_TO_WAIT_FOR cars to send messages that they want to cross
        while (cntCarsToRelease < CARS_TO_WAIT_FOR && timeout == 0) {

            if (msgrcv(msqid, (struct msgbuf *)&buf, sizeof(buf)-sizeof(long), smjer + 9999, 0) == -1) {
                if (errno == EINTR) break;
                perror("msgrcv");
                exit(1);
            }

            //store car data
            msqids[cntCarsToRelease] = msqid;
            reg_oznake[cntCarsToRelease] = atoi(buf.mtext);
            cntCarsToRelease ++;
        }

        //turn off alarm
        ualarm(0, 0);
        timeout = 0;

        //release cntCarsToRelease and report
        for (int i = 0; i < cntCarsToRelease; ++i) {

            char text1[] = "Prijeđi";

            memcpy(buf.mtext, text1, strlen(text1) + 1);
            buf.mtype = reg_oznake[i];

            if (msgsnd(msqid, (struct msgbuf *) &buf, strlen(text1) + 1, 0) == -1)
                perror("msgsnd2");
        }

        //wait 1 to 3 seconds
        usleep(rand() % 2000001 + 1000000);

        //report that the cars have crossed the bridge
        for (int i = 0; i < cntCarsToRelease; ++i) {

            char text2[] = "Prešao";

            memcpy(buf.mtext, text2, strlen(text2)+1);
            buf.mtype = reg_oznake[i];

            if (msgsnd(msqid, (struct msgbuf *)&buf, strlen(text2) + 1, 0) == -1)
                perror("msgsnd3");
        }

        //reverse direction
        smjer = (smjer + 1) % 2;
        cnt += cntCarsToRelease;
    }

    //close message queues
    msgctl(msqid1,IPC_RMID, NULL);
    msgctl(msqid2,IPC_RMID, NULL);
}

//cars function for cars that want to cross the brdge
void auto_func(int registarska_oznaka) {

    //wait 100 to 2000 milliseconds
    usleep(rand() % 1900001 + 100000);
    srand(getpid());

    //determine direction at random
    int smjer = rand() & 1;

    struct my_msgbuf buf;
    int msqid;
    key_t key;

    //determine queue used depending on direction
    if (smjer == 0) key = 1;
    else key = 2;

    //open message queue in chosen direction
    if ((msqid = msgget(key, 0600 | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }

    sprintf(buf.mtext, "%d", registarska_oznaka);
    buf.mtype = smjer + 9999;

    //report that you want to cross the bridge
    if (msgsnd(msqid, (struct msgbuf *)&buf, strlen(buf.mtext) + 1, 0) == -1)
        perror("msgsnd1");

    printf("Automobil %d čeka na prelazak preko mosta u smjeru %d\n", registarska_oznaka, smjer);

    //wait to be let through
    if (msgrcv(msqid, (struct msgbuf *)&buf, sizeof(buf)-sizeof(long), registarska_oznaka, 0) == -1) {
        perror("msgrcv");
        exit(1);
    }

    if (strcmp(buf.mtext, "Prijeđi") == 0) {
        printf("Automobil %d se popeo na most\n", registarska_oznaka);
    }

    //wait to pass
    if (msgrcv(msqid, (struct msgbuf *)&buf, sizeof(buf)-sizeof(long), registarska_oznaka, 0) == -1) {
        perror("msgrcv");
        exit(1);
    }

    if (strcmp(buf.mtext, "Prešao") == 0) {
        printf("Automobil %d je prešao most\n", registarska_oznaka);
    }
}

int main(int argc, char *argv[]) {

    if (argc != 2) {
        printf("Neispravan broj argumenata\n");
        return 1;
    }

    if (atoi(argv[1]) < 5 || atoi(argv[1]) > 100) {
        printf("Neispravam broj auta\n");
        return 1;
    }

    //create cars
    for (int i = 0; i < atoi(argv[1]); ++i) {
        if (fork() == 0) {
            auto_func(i + 1);
            return 0;
        }
    }

    //create semaphore
    semafor(atoi(argv[1]));

    return 0;
}
