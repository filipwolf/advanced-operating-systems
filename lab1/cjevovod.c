#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <sys/wait.h>

#define NUMOFENTRIES 5

//database entry struct
struct db_entry {
    int proc_id;
    int log_clock;
    int num_of_entries;
};

//message struct
struct message {
    char type[7];
    int clock;
    int id;
};


//process driver code
void process(struct db_entry *database, int fd[], int proc_id, int num_of_processes) {

    srand(getpid());

    struct message msg;
    int rec_cnt = 0;
    int log_clock = 0;
    int req_clock ;

    //close unneeded pipes
    close(fd[2 * proc_id + 1]);
    for (int i = 0; i < num_of_processes - 1; ++i)
        if (i != proc_id) close(fd[2 * i]);

    //loop for how many times to enter critical section
    for (int i = 0; i < NUMOFENTRIES; ++i) {

        int received = 0;
        int message_queue[9] = {0};
        int rec_clocks[9] = {0};
        int message_queue_size = 0;
        int cnt = 0;

        sprintf(msg.type, "Zahtjev");
        msg.clock = log_clock;
        msg.id = proc_id;
        req_clock = log_clock;

        //send request messages
        for (int j = 0; j < num_of_processes; ++j)
            if (j != proc_id) {
                printf("Proces %d šalje poruku %s procesu %d\n", proc_id, msg.type, j);
                write(fd[2 * j + 1], &msg, sizeof (struct message));
            }

        //receive response messages
        while (received < num_of_processes - 1) {
            read(fd[2 * proc_id], &msg, sizeof (struct message));

            char str[7];
            sprintf(str, "%s", msg.type);
            int rec_clock = msg.clock;
            int rec_id = msg.id;

            //check if message is of type request
            if (strcmp(str, "Zahtjev") == 0) {

                //condition when to send response
                if (req_clock > rec_clock || (req_clock == rec_clock && proc_id > rec_id)) {
                    sprintf(msg.type, "Odgovor");
                    msg.id = proc_id;

                    printf("Proces %d šalje poruku %s procesu %d\n", proc_id, msg.type, rec_id);
                    write(fd[2 * rec_id + 1], &msg, sizeof (struct message));

                //or store for later processing
                } else {
                    message_queue[message_queue_size] = rec_id;
                    rec_clocks[message_queue_size] = rec_clock;
                    message_queue_size ++;
                }

                rec_cnt ++;

            //if not, it is a response, update the counter
            } else received ++;

            log_clock = log_clock > rec_clock ? log_clock + 1 : rec_clock + 1;
        }

        //critical section
        database[proc_id].log_clock = log_clock;
        database[proc_id].num_of_entries ++;

        printf("\nProces %d je u kritičnom odsječku:\n", proc_id);
        for (int j = 0; j < num_of_processes; ++j)
            printf("    id: %d, log_clock: %d, num_of_entries: %d\n", database[j].proc_id, database[j].log_clock, database[j].num_of_entries);
        printf("\n");

        usleep(rand() % 1900001 + 100000);
        //end of critical section

        //answer stored requests
        while (message_queue_size != cnt) {
            sprintf(msg.type, "Odgovor");
            msg.clock = rec_clocks[cnt];
            msg.id = proc_id;

            printf("Proces %d šalje poruku %s procesu %d\n", proc_id, msg.type, message_queue[cnt]);
            write(fd[2 * message_queue[cnt] + 1], &msg, sizeof (struct message));

            cnt ++;
        }
    }

    //after 5 critical section entries, grant permits to other processes
    while (rec_cnt < NUMOFENTRIES * (num_of_processes - 1)) {
        read(fd[2 * proc_id], &msg, sizeof (struct message));

        char str[7];
        sprintf(str, "%s", msg.type);
        int rec_clock = msg.clock;
        int rec_id = msg.id;

        //always send response
        if (strcmp(str, "Zahtjev") == 0) {
            sprintf(msg.type, "Odgovor");
            msg.id = proc_id;

            printf("Proces %d šalje poruku %s procesu %d\n", proc_id, msg.type, rec_id);
            write(fd[2 * rec_id + 1], &msg, sizeof (struct message));

            rec_cnt ++;
        }

        log_clock = log_clock > rec_clock ? log_clock + 1 : rec_clock + 1;
    }

    //close pipes
    close(fd[2 * proc_id]);
    for (int i = 0; i < num_of_processes - 1; ++i) {
        if (i != proc_id) {
            close(fd[2 * i] + 1);
        }
    }
}

int main(int argc, char *argv[]) {

    if (argc != 2) {
        printf("Neispravan broj argumenata\n");
        return 1;
    }

    int num_of_processes = atoi(argv[1]);

    if (num_of_processes < 3 || num_of_processes > 10) {
        printf("Neispravam broj procesa\n");
        return 1;
    }

    int shm_id;
    int fd[2 * num_of_processes];

    //allocate shared section
    shm_id = shmget(IPC_PRIVATE, num_of_processes * sizeof(struct db_entry), 0600);
    struct db_entry *database =  shmat(shm_id, NULL, 0);

    //initialize shared database
    for (int i = 0; i < num_of_processes; i++) {
        pipe(&fd[2 * i]);
        database[i].proc_id = i;
        database[i].log_clock = 0;
        database[i].num_of_entries = 0;
    }

    //create child processes
    for (int i = 0; i < num_of_processes; ++i) {
        if (fork() == 0) {
            process(database, fd, i, num_of_processes);
            return 0;
        }
    }

    //wait until all child processes are done
    for (int i = 0; i < num_of_processes; ++i) {
        wait(NULL);
    }

    //close shared database
    shmdt(database);
    shmctl(shm_id, IPC_RMID, NULL);

    exit(0);
}
