import json
from base64 import b64encode, b64decode

from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Cipher import DES3
from Crypto.Random import get_random_bytes
from Crypto.Util.Padding import pad, unpad


class AESMethod:

    def __init__(self, keySize, cryptoAlgo, key=bytes(0)):
        self.iv = get_random_bytes(16)
        if key == bytes(0):
            self.key = get_random_bytes(int(keySize/8))
        else:
            self.key = key
        self.cryptoAlgo = cryptoAlgo

    def encrypt(self, message):

        message = str.encode(message)

        if self.cryptoAlgo == "ECB":
            obj = AES.new(self.key, AES.MODE_ECB)
            return b64encode(obj.encrypt(pad(message, AES.block_size))).decode('utf-8')
        elif self.cryptoAlgo == "CBC":
            obj = AES.new(self.key, AES.MODE_CBC, self.iv)
            retBytes = obj.encrypt(pad(message, AES.block_size))
            iv = b64encode(obj.iv).decode('utf-8')
            ct = b64encode(retBytes).decode('utf-8')
            return json.dumps({'iv': iv, 'ciphertext': ct})

    def decrypt(self, encryptedText):

        if self.cryptoAlgo == "ECB":
            obj = AES.new(self.key, AES.MODE_ECB)
            return unpad(obj.decrypt(b64decode(encryptedText)), AES.block_size).decode()
        elif self.cryptoAlgo == "CBC":
            b64 = json.loads(encryptedText)
            obj = AES.new(self.key, AES.MODE_CBC, b64decode(b64["iv"]))
            return unpad(obj.decrypt(b64decode(b64["ciphertext"])), AES.block_size).decode()


class DES3Method:

    def __init__(self, cryptoAlgo,  key=bytes(0)):
        self.iv = Random.new().read(DES3.block_size)
        if key == bytes(0):
            self.key = DES3.adjust_key_parity(get_random_bytes(24))
        else:
            self.key = key
        self.cryptoAlgo = cryptoAlgo

    def encrypt(self, message):

        message = str.encode(message)

        if self.cryptoAlgo == "ECB":
            obj = DES3.new(self.key, DES3.MODE_ECB)
            return b64encode(obj.encrypt(pad(message, DES3.block_size))).decode('utf-8')
        elif self.cryptoAlgo == "CBC":
            obj = DES3.new(self.key, DES3.MODE_CBC, self.iv)
            retBytes = obj.encrypt(pad(message, DES3.block_size))
            iv = b64encode(obj.iv).decode('utf-8')
            ct = b64encode(retBytes).decode('utf-8')
            return json.dumps({'iv': iv, 'ciphertext': ct})
        if self.cryptoAlgo == "OFB":
            obj = DES3.new(self.key, DES3.MODE_OFB, self.iv)
        if self.cryptoAlgo == "CFB":
            obj = DES3.new(self.key, DES3.MODE_CFB, self.iv)
        if self.cryptoAlgo == "CTR":
            obj = DES3.new(self.key, DES3.MODE_CTR)

    def decrypt(self, encryptedText):

        if self.cryptoAlgo == "ECB":
            obj = DES3.new(self.key, DES3.MODE_ECB)
            return unpad(obj.decrypt(b64decode(encryptedText)), DES3.block_size).decode()
        elif self.cryptoAlgo == "CBC":
            b64 = json.loads(encryptedText)
            obj = DES3.new(self.key, DES3.MODE_CBC, b64decode(b64["iv"]))
            return unpad(obj.decrypt(b64decode(b64["ciphertext"])), DES3.block_size).decode()
        if self.cryptoAlgo == "OFB":
            obj = DES3.new(self.key, DES3.MODE_OFB, self.iv)
        if self.cryptoAlgo == "CFB":
            obj = DES3.new(self.key, DES3.MODE_CFB, self.iv)
        if self.cryptoAlgo == "CTR":
            obj = DES3.new(self.key, DES3.MODE_CTR, self.iv)


if __name__ == "__main__":

    keySize = 0

    print("Upišite koji algoritam želite [AES, 3DES]: ")
    algo = input()

    if algo == "AES":
        print("Koju veličinu ključa želite? [128, 192, 256]: ")
        keySize = int(input())

    print("Izaberite način kriptiranja [ECB, CBC]: ")
    cryptoAlgo = input()

    if algo == "AES":
        method = AESMethod(keySize, cryptoAlgo)
        f = open("key", "w")
        f.write("---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Key file\n\nMethod:\n    AES\n\nKey length:\n    "
                + str(hex(keySize)) + "\n\nSecret key:\n    " + str(method.key.hex()) + "\n---END OS2 CRYPTO DATA---")
        f.close()
    else:
        method = DES3Method(cryptoAlgo)
        f = open("key", "w")
        f.write("---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Key file\n\nMethod:\n    3DES\n\nSecret key:\n    "
                + str(method.key.hex()) + "\n---END OS2 CRYPTO DATA---")
        f.close()

    message = "It is all a product of causality."
    print("Original message is: ", message)
    encryptedText = method.encrypt(message)
    print("The encrypted text", encryptedText)
    if algo == "AES":
        f = open("text", "w")
        f.write("---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Crypted file\n\nMethod:\n    AES\n\nData:\n    "
                + str(encryptedText) + "\n---END OS2 CRYPTO DATA---")
        f.close()
    else:
        f = open("text", "w")
        f.write("---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Crypted file\n\nMethod:\n    3DES\n\nData:\n    "
                + str(encryptedText) + "\n---END OS2 CRYPTO DATA---")
        f.close()

    f = open("key", "r")
    lines = f.readlines()

    keySize = 0
    method = 0
    key = 0
    flag = False
    for line in lines:
        if flag is True:
            key = bytearray.fromhex(line.strip())
            flag = False
        if line == "    AES\n":
            method = "AES"
        elif line == "    3DES\n":
            method = "3DES"
        if line == "    0x80\n":
            keySize = 128
        elif line == "    0xc0\n":
            keySize = 192
        else:
            keySize = 256
        if line == "Secret key:\n":
            flag = True

    if method == "AES":
        method = AESMethod(keySize, cryptoAlgo, key)

        f = open("text", "r")
        lines = f.readlines()
        flag = False
        data = 0
        for line in lines:
            if flag is True:
                data = line.strip().rstrip()
                flag = False
            if line == "Data:\n":
                flag = True
    else:
        method = DES3Method(cryptoAlgo, key)

        f = open("text", "r")
        lines = f.readlines()
        flag = False
        data = 0
        for line in lines:
            if flag is True:
                data = line.strip().rstrip()
                flag = False
            if line == "Data:\n":
                flag = True

    decryptedText = method.decrypt(data)
    print("The decrypted text is: ", decryptedText)
