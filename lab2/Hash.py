from Crypto.Hash import SHA256
from Crypto.Hash import SHA512


def sha256(data):
    return SHA256.new(data), 256


def sha512(data):
    return SHA512.new(data), 512


if __name__ == "__main__":
    hash_object = SHA256.new(data=b'First')
    print(hash_object.digest())
    print(hash_object.hexdigest())
