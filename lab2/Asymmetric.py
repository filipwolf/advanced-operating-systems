from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA


class RSAMethod:

    def __init__(self, public, modulus, private=bytes(0)):
        if private == bytes(0):
            self.key = RSA.construct((modulus, public))
        else:
            self.key = RSA.construct((modulus, public, private))

    def encrypt(self, data):
        cipher = PKCS1_OAEP.new(self.key)
        return cipher.encrypt(data)

    def decrypt(self, data):
        cipher = PKCS1_OAEP.new(self.key)
        return cipher.decrypt(data)


if __name__ == "__main__":

    print("Upišite veličinu ključa za RSA [1024, 2048, 4096]: ")
    keySize = int(input())

    key = RSA.generate(keySize)

    f = open("RSA_key_private", "w")
    f.write("---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Private key\n\nMethod:\n    RSA\n\nKey length:\n    "
            + str(hex(keySize)) + "\n\nModulus:\n    " + str(hex(key.n)) + "\n\nPrivate exponent:\n    "
            + str(hex(key.d)) + "\n\n---END OS2 CRYPTO DATA---")
    f.close()

    f = open("RSA_key_public", "w")
    f.write("---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Private key\n\nMethod:\n    RSA\n\nKey length:\n    "
            + str(hex(keySize)) + "\n\nModulus:\n    " + str(hex(key.n)) + "\n\nPrivate exponent:\n    "
            + str(hex(key.e)) + "\n\n---END OS2 CRYPTO DATA---")
    f.close()

    method = RSAMethod(key.d, key.n)
