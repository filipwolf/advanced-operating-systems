from Crypto.PublicKey import RSA

import Hash
from Asymmetric import RSAMethod

if __name__ == "__main__":

    print("Upišite veličinu ključa za RSA [1024, 2048, 4096]: ")
    keySize = int(input())

    key = RSA.generate(keySize)

    f = open("RSA_key_private", "w")
    f.write("---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Private key\n\nMethod:\n    RSA\n\nKey length:\n    "
            + str(hex(keySize)) + "\n\nModulus:\n    " + str(hex(key.n)) + "\n\nPrivate exponent:\n    "
            + str(hex(key.d)) + "\n\n---END OS2 CRYPTO DATA---")
    f.close()

    f = open("RSA_key_public", "w")
    f.write("---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Private key\n\nMethod:\n    RSA\n\nKey length:\n    "
            + str(hex(keySize)) + "\n\nModulus:\n    " + str(hex(key.n)) + "\n\nPrivate exponent:\n    "
            + str(hex(key.e)) + "\n\n---END OS2 CRYPTO DATA---")
    f.close()

    method = RSAMethod(key.e, key.n, key.d)

    print("Upišite SHA metodu koju želite [SHA256, SHA512]: ")
    shaMethod = input()

    message = "It is all a product of causality."
    print("Original message is: ", message)

    if shaMethod == "SHA256":
        hashObject, keySize2 = Hash.sha256(str.encode(message))
    else:
        hashObject, keySize2 = Hash.sha512(str.encode(message))

    encryptedHash = method.encrypt(hashObject.digest())

    if shaMethod == "SHA256":

        f = open("signature", "w")
        f.write("---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Signature\n\nMethod:\n    SHA256\n    RSA\n\nKey "
                "length:\n " + str(hex(256)) + "\n    " + str(hex(keySize)) + "\n\nSignature:\n    "
                + str(encryptedHash.hex()) + "\n\n---END OS2 CRYPTO DATA---")
        f.close()

    else:
        f = open("signature", "w")
        f.write("---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Signature\n\nMethod:\n    SHA512\n    RSA\n\nKey "
                "length:\n " + str(hex(512)) + "\n    " + str(hex(keySize)) + "\n\nSignature:\n    "
                + str(encryptedHash.hex()) + "\n\n---END OS2 CRYPTO DATA---")
        f.close()

    f = open("message", "w")
    f.write(
        "---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Message\n\nMessage:\n    " + message
        + "\n\n---END OS2 CRYPTO DATA---")
    f.close()

    f = open("signature", "r")
    lines = f.readlines()

    keySize1 = 0
    keySize2 = 0
    signature = 0
    method2 = 0
    flag = False

    for line in lines:
        if flag is True:
            signature = bytearray.fromhex(line.strip())
            flag = False
        if line == "    SHA256\n":
            method2 = "SHA256"
        elif line == "    SHA512\n":
            method2 = "SHA512"
        if line == "    0x100\n":
            keySize1 = 256
        elif line == "    0x200\n":
            keySize1 = 512
        elif line == "    0x400\n":
            keySize2 = 1024
        elif line == "    0x800\n":
            keySize2 = 2048
        else:
            keySize2 = 4096
        if line == "Signature:\n":
            flag = True

    f = open("message", "r")
    lines = f.readlines()

    for line in lines:
        if flag is True:
            message = line.strip().rstrip()
            flag = False
        if line == "Message:\n":
            flag = True

    hashRec = method.decrypt(signature)
    if method2 == "SHA256":
        hashNew, keySize2 = Hash.sha256(str.encode(message))
    else:
        hashNew, keySize2 = Hash.sha512(str.encode(message))

    print("The received message is: ", message)

    if hashRec == hashNew.digest():
        print("The signature is valid")
    else:
        print("The signature is invalid")
