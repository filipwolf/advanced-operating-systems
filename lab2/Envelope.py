from Crypto.PublicKey import RSA

from Asymmetric import RSAMethod
from Symmetric import AESMethod, DES3Method

if __name__ == "__main__":

    keySize1 = 0

    print("Upišite koji algoritam želite [AES, 3DES]: ")
    algo = input()

    if algo == "AES":
        print("Koju veličinu ključa želite? [128, 192, 256]: ")
        keySize1 = int(input())

    print("Izaberite način kriptiranja [ECB, CBC]: ")
    cryptoAlgo = input()

    if algo == "AES":
        method = AESMethod(keySize1, cryptoAlgo)
        f = open("key", "w")
        f.write("---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Key file\n\nMethod:\n    AES\n\nKey length:\n    "
                + str(hex(keySize1)) + "\n\nSecret key:\n    " + str(method.key.hex()) + "\n---END OS2 CRYPTO DATA---")
        f.close()
    else:
        method = DES3Method(cryptoAlgo)
        f = open("key", "w")
        f.write("---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Key file\n\nMethod:\n    3DES\n\nSecret key:\n    "
                + str(method.key.hex()) + "\n---END OS2 CRYPTO DATA---")
        f.close()

    message = "It is all a product of causality."
    print("Original message is: ", message)
    encryptedText = method.encrypt(message)
    print("The encrypted text", encryptedText)

    print("Upišite veličinu ključa za RSA [1024, 2048, 4096]: ")
    keySize2 = int(input())

    key = RSA.generate(keySize2)

    f = open("RSA_key_private", "w")
    f.write("---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Private key\n\nMethod:\n    RSA\n\nKey length:\n    "
            + str(hex(keySize2)) + "\n\nModulus:\n    " + str(hex(key.n)) + "\n\nPrivate exponent:\n    "
            + str(hex(key.d)) + "\n\n---END OS2 CRYPTO DATA---")
    f.close()

    f = open("RSA_key_public", "w")
    f.write("---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Private key\n\nMethod:\n    RSA\n\nKey length:\n    "
            + str(hex(keySize2)) + "\n\nModulus:\n    " + str(hex(key.n)) + "\n\nPrivate exponent:\n    "
            + str(hex(key.e)) + "\n\n---END OS2 CRYPTO DATA---")
    f.close()

    method2 = RSAMethod(key.e, key.n, key.d)
    encryptedKey = method2.encrypt(method.key)

    if algo == "AES":
        f = open("envelope", "w")
        f.write(
            "---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Envelope\n\nMethod:\n    AES\n    RSA\n\nKey length:\n    "
            + str(hex(keySize1)) + "\n    " + str(hex(keySize2)) + "\n\nEnvelope data:\n    " + str(encryptedText)
            + "\n\nEnvelope crypt key:\n    " + str(encryptedKey.hex()) + "\n\n---END OS2 CRYPTO DATA---")
        f.close()

    else:
        f = open("envelope", "w")
        f.write(
            "---BEGIN OS2 CRYPTO DATA---\nDescription:\n    Envelope\n\nMethod:\n    3DES\n    RSA\n\nKey length:\n    "
            + str(hex(keySize2)) + "\n\nEnvelope data:\n    " + str(encryptedText) + "\n\nEnvelope crypt key:\n    "
            + str(encryptedKey.hex()) + "\n\n---END OS2 CRYPTO DATA---")
        f.close()

    f = open("envelope", "r")
    lines = f.readlines()

    keySize1 = 0
    keySize2 = 0
    encryptedKey = 0
    encryptedText = 0
    flag1 = False
    flag2 = False

    for line in lines:
        if flag1 is True:
            encryptedText = line.strip().rstrip()
            flag1 = False
        if flag2 is True:
            encryptedKey = bytearray.fromhex(line.strip())
            flag2 = False
        if line == "    AES\n":
            method = "AES"
        elif line == "    3DES\n":
            method = "3DES"
        if line == "    0x80\n":
            keySize1 = 128
        elif line == "    0xc0\n":
            keySize1 = 192
        elif line == "    0x100\n":
            keySize1 = 256
        elif line == "    0x400\n":
            keySize2 = 1024
        elif line == "    0x800\n":
            keySize2 = 2048
        else:
            keySize2 = 4096
        if line == "Envelope data:\n":
            flag1 = True
        if line == "Envelope crypt key:\n":
            flag2 = True

    key = method2.decrypt(encryptedKey)

    if method == "AES":
        method = AESMethod(keySize1, cryptoAlgo, key)
    else:
        method = DES3Method(cryptoAlgo, key)

    decryptedText = method.decrypt(encryptedText)
    print("The decrypted text is: ", decryptedText)
